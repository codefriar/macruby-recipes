class ControlViaScriptingBridge
	#-- Goal: Generic class to access Scripting bridge enabled Mac App Bundles. ---
	#-- This example recipe will override the instance variable with iTunes for ---
	#---- the sake of example

	framework "ScriptingBridge"  # framework is a MacRuby specific keyword that 
															 # loads Cocoa frameworks, in this case 
															 # ScriptingBridge.

	#-- iVars
	attr_accessor :application_handle, :application_name

	#-- Instantiating this object without a bundle identifier will default this to iTunes
	def initialize app_bundle_identifier = "com.apple.iTunes" 
		@application_name = app_bundle_identifier
		#-- This next line utilizes the SBApplication object from the ScriptingBridge Framework
		#---- there are other selector/methods to grab a bundle say by filesystem path but
		#---- the recommend method is applicationWithBundleIdentifier
		@application_handle = SBApplication.applicationWithBundleIdentifier(@application_name)
		#-- One of the most beloved features of ruby is it's introspective nature. For instance
		#---- Obj.methods will give you a list of all the methods avialable to the object. 
		#---- MacRuby complicates this slightly by not clearly documenting how to limit the method
		#---- list to Object-C methods. This is the secret sauce.
		@bridge_methods = @application_handle.methods(false, true) - @application_handle.methods(false)
		dynamically_create_bridge_methods
	end

	def dynamically_create_bridge_methods
		#-- This creates pass-through instance methods for convience sake. 
		#---- We dynamically define a method on this instance for each method available to the @application_handle object
		#---- Thus, if we instantiate this class with, itunes = ControlViaScriptingBridge.new("com.apple.iTunes")
		#---- we can then execute itunes.sources.select {|source| source.name == "Library"} directly to access the source
		#---- object and it's methods.
		@bridge_methods.each do |m|
			ControlViaScriptingBridge.send :define_method, m.to_sym do |*args|
				@application_handle.send(m, *args)
			end
		end
	end

end

#-- Example ussage of this class ----------------------------------------------
# #!/usr/local/bin/macruby
#
# require './Control_Mac_Apps_via_Scripting_Bridge'
#
# itunes = ControlViaScriptingBridge.new("com.apple.iTunes")
#
# favsongs = itunes.sources  
#   .find {|s| s.name == "Library"}  
#   .playlists.find {|p| p.name == "My Top Rated"}  
#  
#
#   favsongs.playOnce(false)
#-- End Example ---------------------------------------------------------------