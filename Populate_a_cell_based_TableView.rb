 class PopulateTableView
  #-- Note: This demonstrates how to populate NSCell based NSTableViews. NSView backed 
  #--  NSTableViews are in another demonstration coming later.
  #-- File is split into two sections, methods to manipulate the Tableview itself and
  #--  perhaps more importantly, the methods fulfilling the protocol to populate a 
  #--  TableView and respond to a selection.

  attr_accessor :table_view_handle #outlet to be linked to the actual tableView for controling table wide events
  attr_accessor :data_array #array of objects that the tableview will display

  def populate(array_of_data)
    @data_array = array_of_data
    reload!
  end

  def reload!
    @table_view_handle.reloadData #this isn't bad ruby method naming, it's Objc-in-ruby-invocation
  end

  #-- TableView Informal Protocol ----------------------------------------------------------------
  #-- TableView requires an Array of objects to display ------------------------------------------
  #-- Identify the number of objects (rows) in the array -----------------------------------------
  def numberOfRowsInTableView view
    @data_array.size rescue 0
  end
  
  #-- TableView Informal Protocol *getter* -------------------------------------------------------
  #-- objectValueForTableColumn:column, row:index returns the object at Array[index].property ----
  #-- In Xcode use the object's property names as the column's User Interface Item Identifier ----
  #-- In other words, if your array contained Name objects like this -----------------------------
  # class Name
  #   attr_accessor :first, :last
  # end
  #-- You would name your TableView columns |first|last| in xCode --------------------------------
  #-- This naming convention allows xcode to use this next method to populate the table by asking-
  #-- what's at column "first" or "last" of row "1" for table view's data_array?
  def tableView(view, objectValueForTableColumn:column, row:index)
    row = @data_array[index]
    row.send column.identifier.to_sym
  end
  
  #-- TableView Row Was selected! ----------------------------------------------------------------
  #-- TableView's support row and column selection. this method is fired when cocoa detects that -
  #-- the user has selected a row.
  def tableViewSelectionDidChange notification
    puts "Recieved view selection change notification. Row Index: #{@data_array_view.selectedRow}"
    #-- you can call custom code / methods etc. here ---------------------------------------------
  end
  
  #-- End TableView Informal Protocol ------------------------------------------------------------
end