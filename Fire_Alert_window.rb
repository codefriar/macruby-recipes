class FireAlertWindow
	#-- Goal: Simple demo class to demonstrate how to throw an NSalert window
	#---- with both an "ok" and a "cancel" button. 

	#-- just three accessors, for the alert title, info text and the return value.
	attr_accessor :alert_text, :title, :return_value

	#-- a basic initializer to wrap all this all up.
	def initialize(alert_text, title)
		@alert_text = alert_text
		@title = title
		fire_alert nil
	end

  def fire_alert sender
  	#-- This method both sets up and displays the alert.

  	#-- we want to ensure that we have a message 
  	if (@alert_text.stringValue.nil? || @alert_text.stringValue == "")
  		@alert_text.stringValue =  "I am an alert box, you forgot to give me text!"
  	end
    
    #-- and a title
    if (@title.stringValue.nil? || @title.stringValue == "") 
	    @title.stringValue = "Default Title"
	  end

    #-- this sets up the alert object
    alert = NSAlert.alertWithMessageText(@title.stringValue, #first argument is the title, as a string! not an object.
                                         defaultButton: "OK", #sets the text label of the default button *** return Const 1 ***
                                         alternateButton: "Cancel", #sets the text label of the alternate (normally cancel) button *** return Const 0 ***
                                         otherButton: nil, #You can set yet a third button here. *** return Const -1 ***
                                         informativeTextWithFormat: @alert_text.stringValue) # This is the informative text 
    
    @return_value = alert.runModal #this captures the constant integer value of which button the user selected.
  end

end