class FileOrDirectoryOpenSheet
	#-- Goal: Define a class, whose method gives us a nice file-open or directory choosing panel / sheet.
	#-- Note: When using this code, you must either loop over @results or call @results.first
	
	attr_accessor :can_choose_files, :can_choose_directories, :can_choose_multiple_things
	attr_reader :results

  def browse_for_directory sender
    #-- Create the File Open Dialog class.
    directory_chooser = NSOpenPanel.openPanel 																					         
    #-- Disable the selection of files in the dialog.
    directory_chooser.canChooseFiles = (@can_choose_files.downcase == 'true')
    #-- Enable the selection of directories in the dialog.
    directory_chooser.canChooseDirectories = (@can_choose_directories.downcase == 'true')
    #-- Disable the selection of multiple items in the dialog.
    directory_chooser.allowsMultipleSelection = (@can_choose_multiple_things.downcase == 'true')

    #-- Display the dialog and process the selected folder / file
    if directory_chooser.runModalForDirectory(nil, file:nil) == NSOKButton
    	@results = directory_chooser.filenames
    end
  end

end