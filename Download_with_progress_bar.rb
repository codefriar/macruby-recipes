class DownloadWithProgressBar
	#-- Prerequisites:
	#---- 1: a NSPanel configured with at least an NSProgressIndicator (Horizontal, Non-Indeterminate)

	#-- Goal: to establish a NSPanel sheet that will display a progress bar indicating the progress of a file download.
	#-- Xcode handles/outlets
	attr_accessor :download_panel #IBoutlet to be connectd to the NSPanel Object / Nib in your IB Object Dock.
	attr_accessor :progress_indicator #IBOutlet to be connected to the NSProgressIndicator on the @download_panel Nib.
	attr_accessor :download_title #(optional) IBOutlet to be connected to a NSLabel object on the @download_panel Nib.

	#-- Constants to make this example work / make sense.
	FILE_TO_DOWNLOAD = "http://www.mozilla.org/en-US/products/download.html?product=firefox-10.0&os=osx&lang=en-US"
	LOCAL_PATH = "~/" #Considering this is macruby ... a unix based path assumption of a safe place to write seems ... safe. right?

	def the_one_method_to_start_it_all(url = FILE_TO_DOWNLOAD, local_path = LOCAL_PATH)
		#-- this next method call starts the display of the sheet. We'll dismiss the sheet later in a different segment of code.
		NSApp.beginSheet(@download_panel,                   #This is the IBoutlet link to the actual NSPanel object to use as the sheet!
										 modalForWindow:NSApp.mainWindow,   #You don't have to use NSApp.mainWindow, but you do need to specify a window here
										 modalDelegate:self,                #this modal should use this class as it's delegate
										 didEndSelector:nil,                #selector (method) to run when the sheet is finished. --think callback--
										 contextInfo:nil)                   #i have no idea.
		
		#-- (Optional) Set the sheet's title
		self.download_title.stringValue = "Downloading #{url}" 
		#-- the above line could also be written @download_title.stringValue However self. syntax fires KVO notifications
		#-- KVO notifications become important when using Cocoa bindings, a powerful but voodoo-esque technology.

		#-- Start the download using the below protocol
		download(url, local_path);
	end

	#-- Protocols ----------------------------------------------------------------

  #-- Start Download with Progress Bar Informal Protocol -----------------------
  #-- helper to setup request and create a delegate to monitor the transfer ----
  def download(incoming_url, local_path, delegator)
    url = NSURL.URLWithString(incoming_url)
    request = NSURLRequest.requestWithURL(url)
    @path_to_save_response = local_path
    #-- For some reason, I've had inconsistent results with connectionWithRequest when it's invoked from a non
    #---- main thread. this dispatch call ensure's that the call is made on the main thread. Normally this is
    #---- a bad idea (tm) but NSURLConnection.connectionWithRequest does it's work on it's own thread anyway so 
    #---- it's all good. we just have to start it on the main thread for some reason.
    Dispatch::Job.new(Dispatch::Queue.main) do
      @connection = NSURLConnection.connectionWithRequest(request, delegate:delegator)
    end
  end
  
  #-- Delegate Method -- Download is Finished ----------------------------------
  def connectionDidFinishLoading(connection)
    if @receivedData && @receivedData.writeToFile(@path_to_save_response, atomically:true)
      Dispatch::Job.new {
      	#-- Async dismiss the sheet by ending it and then ordering off the screen.
        NSApp.endSheet(@downloading_panel)
        @downloading_panel.orderOut(self)
      }
    else
      raise "FAILED TO SAVE FILE!"
    end
    @recievedData = nil
  end
  
  #-- Delegate Method -- Connection --------------------------------------------
  #-- Requires the use of the ObjC selector didReceiveResponse:response in 
  #-- method sig -------------------------------------------------------------
  def connection(connection, didReceiveResponse:response)
    expected_size = response.expectedContentLength.to_f
    #set the max value of the progress bar to the size of the file. Makes it darn'd easy to draw the progress bar!
    if expected_size > 0
      @progress_indicator.indeterminate = false
      @progress_indicator.maxValue = expected_size.to_f
    end
  end
  
  #-- Delegate Method -- Connection --------------------------------------------
  #-- This delegate method is called for every chunk of data recieved ----------
  def connection(connection, didReceiveData:receivedData)
    @receivedData ||= NSMutableData.new
    @receivedData.appendData(receivedData)
    @progress_indicator.incrementBy(receivedData.length.to_f)
  end
  #-- End Download With Progress Bar Informal Protocol -------------------------
end